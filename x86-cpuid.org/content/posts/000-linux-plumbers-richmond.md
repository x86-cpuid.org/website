---
title: "Linux Plumbers Conference, Richmond"
date: 2023-11-29T12:27:39+01:00
author: "Ahmed S. Darwish <darwi@linutronix.de>"
tags: ["Richmond", "LPC", "x86-cpuid", "video"]
draft: false
---

During this year's Linux Plumbers Conference, we've introduced the
x86-cpuid project and laid out its rationale.

You can check the talk's
[proposal](https://lpc.events/event/17/contributions/1511/),
[PDF](https://lpc.events/event/17/contributions/1511/attachments/1324/2735/lpc_2023_standardizing_CPUID_data_across_x86_ecosystem.pdf),
and [uploaded video](http://www.youtube.com/watch?v=owZ-rXsPLh4).

As is typical for Plumbers, multiple maintainers related to the x86
ecosystem were in attendance.  Overall, the atmosphere regarding the
project was quite positive, and we are confident in what was done so far.

We shall update you soon regarding further developments.
